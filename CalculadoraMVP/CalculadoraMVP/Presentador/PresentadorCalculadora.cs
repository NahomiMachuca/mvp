using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; //para mostrar el mensaje
using CalculadoraMVP.Modelo; //Agregamos el modelo y las vistas 
using CalculadoraMVP.Vista;


namespace CalculadoraMVP.Presentador
{
    class PresentadorCalculadora
    {
        // se crean los objetos del modelo
        Calculadora calcula = new Calculadora();
        //y se crean las intancias para la interfaz 
        private InterfaceCalculadora CalculadoraVista;
        //se crea el contructor 
        public PresentadorCalculadora (InterfaceCalculadora vista)
        {
            CalculadoraVista = vista;
        }

        public void ModeloYVista()
        {
            calcula.PrimerNumero = Convert.ToDouble(CalculadoraVista.TxtPrimerNumero); //Convertir el dato string a double
            calcula.SegundoNumero = Convert.ToDouble(CalculadoraVista.TxtSegundoNumero);
        }

        public void CalcularSuma()
        {
            //Asigna los valores
            ModeloYVista();
            //Se dirige al metodo calcular suma y asigna el resultado de ese calculo a la propiedad TxtResultado
            CalculadoraVista.TxtResultado = calcula.CalcularSuma().ToString();           
        }

        public void CalcularResta()
        {
            //Asigna los valores
            ModeloYVista();
            //Se dirige al metodo calcular resta y asigna el resultado de ese calculo a la propiedad TxtResultado
            CalculadoraVista.TxtResultado = calcula.CalcularResta().ToString();
        }
        public void CalcularMultiplicacion()
        {
            //Asigna los valores
            ModeloYVista();
            //Se dirige al metodo calcular Multiplicacion y asigna el resultado de ese calculo a la propiedad TxtResultado
            CalculadoraVista.TxtResultado = calcula.CalcularMultiplicacion().ToString();
        }
        public void CalcularDivision()
        {
            //Asigna los valores
            ModeloYVista();
            //hacemos una condicion 
            if (Convert.ToDouble(CalculadoraVista.TxtSegundoNumero) == 0) //Tambien se lo combierte a double para poder trabajr
            {
                MessageBox.Show("No se puede dividir entre 0");
            }else
            {
                //Se dirige al metodo calcular division y asigna el resultado de ese calculo a la propiedad TxtResultado
                CalculadoraVista.TxtResultado = calcula.CalcularDivision().ToString();
            }
          
        }


    }
}
