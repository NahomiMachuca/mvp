﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraMVP.Modelo
{
    public class Calculadora
    {    //definir propiedades
        public double PrimerNumero { get; set; }
        public double SegundoNumero { get; set; }

        //metodo para sumar 
        public double CalcularSuma()
        {
            return PrimerNumero + SegundoNumero;
        }
        //metodo para la resta 
        public double CalcularResta()
        {
            return PrimerNumero - SegundoNumero;
        }
        //multiplicacion 
        public double CalcularMultiplicacion()
        {
            return PrimerNumero * SegundoNumero;
        }
        //division 
        public double CalcularDivision()
        {
            return PrimerNumero / SegundoNumero;
        }

        //despues de hacer esto ir a vista y crear un nuevo elemento 
        //interface
    }
}
