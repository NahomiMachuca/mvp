﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CalculadoraMVP.Presentador;
using CalculadoraMVP.Vista;

namespace CalculadoraMVP
{
    public partial class Form1 : Form, InterfaceCalculadora
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string TxtPrimerNumero { get => txtPrimerNumero.Text; set => txtPrimerNumero.Text = value; }
        public string TxtSegundoNumero { get => txtSegundoNumero.Text; set => txtSegundoNumero.Text = value; }
        public string TxtResultado { get => txtResultado.Text; set => txtResultado.Text = value; }

        private PresentadorCalculadora presentador;


        private void Form1_Load(object sender, EventArgs e)
        {
             //metodo de extension accesible para aceptar el primer argumento de tipo form1
        }

        private void button3_Click(object sender, EventArgs e)
        {
            presentador = new PresentadorCalculadora(this);
            presentador.CalcularDivision();
        }

        private void buttonSuma_Click(object sender, EventArgs e)
        {
            //Inicializa la variable
            presentador = new PresentadorCalculadora(this);
            //Se dirige al metodo Calcular Suma
            presentador.CalcularSuma();
        }

        private void buttonResta_Click(object sender, EventArgs e)
        {
            presentador = new PresentadorCalculadora(this);
            presentador.CalcularResta();
        }

        private void buttonMultiplicar_Click(object sender, EventArgs e)
        {
            presentador = new PresentadorCalculadora(this);
            presentador.CalcularMultiplicacion();
        }
    }
}
