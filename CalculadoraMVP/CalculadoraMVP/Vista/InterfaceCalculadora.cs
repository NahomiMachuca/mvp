using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraMVP.Vista
{
    interface InterfaceCalculadora
    {   
        //Propiedades     
        string TxtPrimerNumero { get; set; }
        string TxtSegundoNumero { get; set; }
        string TxtResultado { get; set; }
    }
}
